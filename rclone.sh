#!/usr/bin/bash

# server
curl https://rclone.org/install.sh | sudo bash
rclone config

# GUI computer
rclone authorize "drive" "eyJzY29wZSI6ImRyaXZlIn0"
# verification token

rclone listremotes

mkdir /root/gdrive
rclone mount GoogleDrive: /root/gdrive/ -vvv --daemon

# copy example
rclone copy ~/test GoogleDrive:ostechnix
