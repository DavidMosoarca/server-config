#!usr/bin/bash

cat << EOF >> /root/.bashrc
export train_archive_prefix="train_set_vfbha39"
export test_archive_prefix="test_set_xsaft57"
export val_archive_prefix="val_set_bjhfy68"

export AuTSL_dataset_archived_local="/root/bachelor-thesis/AuTSL/archived"

export train_data_rgb_dir="/root/bachelor-thesis/AuTSL/data_rgb/train_rgb/"
export train_data_depth_dir="/root/bachelor-thesis/AuTSL/data_depth/train_depth/"
export test_data_rgb_dir="/root/bachelor-thesis/AuTSL/data_rgb/test_rgb/"
export test_data_depth_dir="/root/bachelor-thesis/AuTSL/data_depth/test_depth/"
export val_data_rgb_dir="/root/bachelor-thesis/AuTSL/data_rgb/val_rgb/"
export val_data_depth_dir="/root/bachelor-thesis/AuTSL/data_depth/val_depth/"

export AuTSL_dataset_data_train="/root/bachelor-thesis/AuTSL/data/train"
export AuTSL_dataset_data_test="/root/bachelor-thesis/AuTSL/data/test"
export AuTSL_dataset_data_val="/root/bachelor-thesis/AuTSL/data/val"

export GoogleDrive_AuTSL_dataset="GoogleDrive:bachelor-diploma-project/AuTSL"
export AuTSL_dataset_archived_drive="$GoogleDrive_AuTSL_dataset"/archived
export AuTSL_dataset_archived_local="/root/bachelor-thesis/AuTSL/archived"

EOF
# sudo source /root/.bashrc
