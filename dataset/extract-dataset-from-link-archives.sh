#!/usr/bin/bash

train_archive_prefix="train_set_vfbha39"
test_archive_prefix="test_set_xsaft57"
val_archive_prefix="val_set_bjhfy68"

AuTSL_dataset_archived_local="/root/AuTSL/archived"

for i in {001..018}; do
  wget -P "$AuTSL_dataset_archived_local" http://158.109.8.102/AuTSL/data/train/"$train_archive_prefix".zip."$i";
  # mv "$AuTSL_dataset_archived_local"/"$train_archive_prefix".zip."$i" "$AuTSL_dataset_archived_local_drive";
done

for i in {001..003}; do
  wget -P "$AuTSL_dataset_archived_local" http://158.109.8.102/AuTSL/data/test/"$test_archive_prefix".zip."$i";
  # mv "$AuTSL_dataset_archived_local"/"$test_archive_prefix".zip."$i" "$AuTSL_dataset_archived_local_drive";
done

for i in {001..003}; do
  wget -P "$AuTSL_dataset_archived_local" http://158.109.8.102/AuTSL/data/validation/"$val_archive_prefix".zip."$i";
  # mv "$AuTSL_dataset_archived_local"/"$val_archive_prefix".zip."$i" "$AuTSL_dataset_archived_local_drive";
done
