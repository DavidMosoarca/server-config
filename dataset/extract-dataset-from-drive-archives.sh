#!/usr/bin/bash

train_archive_prefix="train_set_vfbha39"
test_archive_prefix="test_set_xsaft57"
val_archive_prefix="val_set_bjhfy68"

AuTSL_dataset_data_train="/root/AuTSL/data/train"
AuTSL_dataset_data_test="/root/AuTSL/data/test"
AuTSL_dataset_data_val="/root/AuTSL/data/val"

GoogleDrive_AuTSL_dataset="GoogleDrive:bachelor-diploma-project/AuTSL"
AuTSL_dataset_archived_drive="$GoogleDrive_AuTSL_dataset"/archived
AuTSL_dataset_archived_local="/root/AuTSL/archived"

mkdir -p "$AuTSL_dataset_data_train"
mkdir -p "$AuTSL_dataset_data_test"
mkdir -p "$AuTSL_dataset_data_val"
mkdir -p "$AuTSL_dataset_archived_local"

sudo apt-get update
sudo apt-get install p7zip-full

for i in {001..018}; do
    rclone copy "$AuTSL_dataset_archived_drive"/"$train_archive_prefix".zip."$i" "$AuTSL_dataset_archived_local";
    7z x "$AuTSL_dataset_archived_local"/"$train_archive_prefix".zip."$i" -p"MdG3z6Eh1t" -o"$AuTSL_dataset_data_train"/"$train_archive_prefix"_"$i";
    rm   "$AuTSL_dataset_archived_local"/"$train_archive_prefix".zip."$i";
    mv "$AuTSL_dataset_data_train"/"$train_archive_prefix"_"$i"/train/* "$AuTSL_dataset_data_train";
    rm -rf "$AuTSL_dataset_data_train"/"$train_archive_prefix"_"$i";
done

for i in {001..003}; do
    rclone copy "$AuTSL_dataset_archived_drive"/"$test_archive_prefix".zip."$i" "$AuTSL_dataset_archived_local";
    7z x "$AuTSL_dataset_archived_local"/"$test_archive_prefix".zip."$i" -p"ds6Kvdus3o" -o"$AuTSL_dataset_data_test"/"$test_archive_prefix"_"$i";
    rm   "$AuTSL_dataset_archived_local"/"$test_archive_prefix".zip."$i";
    mv  "$AuTSL_dataset_data_test"/"$test_archive_prefix"_"$i"/test/* "$AuTSL_dataset_data_test";
    rm -rf "$AuTSL_dataset_data_test"/"$test_archive_prefix"_"$i";
done

for i in {001..003}; do
    rclone copy "$AuTSL_dataset_archived_drive"/"$val_archive_prefix".zip."$i" "$AuTSL_dataset_archived_local";
    7z x "$AuTSL_dataset_archived_local"/"$val_archive_prefix".zip."$i" -p"bhRY5B9zS2" -o"$AuTSL_dataset_data_val"/"$val_archive_prefix"_"$i";
    rm   "$AuTSL_dataset_archived_local"/"$val_archive_prefix".zip."$i";
    mv   "$AuTSL_dataset_data_val"/"$val_archive_prefix"_"$i"/val/* "$AuTSL_dataset_data_val";
    rm -rf "$AuTSL_dataset_data_val"/"$val_archive_prefix"_"$i";
done
