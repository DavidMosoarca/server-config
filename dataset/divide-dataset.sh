#!/usr/bin/bash

train_data_rgb_dir="/root/bachelor-thesis/AuTSL/data_rgb/train_rgb/"
train_data_depth_dir="/root/bachelor-thesis/AuTSL/data_depth/train_depth/"
test_data_rgb_dir="/root/bachelor-thesis/AuTSL/data_rgb/test_rgb/"
test_data_depth_dir="/root/bachelor-thesis/AuTSL/data_depth/test_depth/"
val_data_rgb_dir="/root/bachelor-thesis/AuTSL/data_rgb/val_rgb/"
val_data_depth_dir="/root/bachelor-thesis/AuTSL/data_depth/val_depth/"

AuTSL_dataset_data_train="/root/bachelor-thesis/AuTSL/data/train"
AuTSL_dataset_data_test="/root/bachelor-thesis/AuTSL/data/test"
AuTSL_dataset_data_val="/root/bachelor-thesis/AuTSL/data/val"

mkdir -p "$train_data_rgb_dir"
mkdir -p "$train_data_depth_dir"
mkdir -p "$test_data_rgb_dir"
mkdir -p "$test_data_depth_dir"
mkdir -p "$val_data_rgb_dir"
mkdir -p "$val_data_depth_dir"

for val_rgb_files in "$AuTSL_dataset_data_val"/*_color.mp4; do
    mv "$val_rgb_files" "$val_data_rgb_dir";
done
for val_depth_files in "$AuTSL_dataset_data_val"/*_depth.mp4; do
    mv "$val_depth_files" "$val_data_depth_dir";
done

for test_rgb_files in "$AuTSL_dataset_data_test"/*_color.mp4; do
    mv "$test_rgb_files" "$test_data_rgb_dir";
done
for test_depth_files in "$AuTSL_dataset_data_test"/*_depth.mp4; do
    mv "$test_depth_files" "$test_data_depth_dir";
done

for train_rgb_files in "$AuTSL_dataset_data_train"/*_color.mp4; do
    mv "$train_rgb_files" "$train_data_rgb_dir";
done
for train_depth_files in "$AuTSL_dataset_data_train"/*_depth.mp4; do
    mv "$train_depth_files" "$train_data_depth_dir";
done

rm -rf /content/AuTSL/data_depth
